#include <boarddefs.h>
#include <IRremote.h>
#include <IRremoteInt.h>
#include <ir_Lego_PF_BitStreamEncoder.h>





#include "motorController.h"
#include "sensorArray.h"


sensorArray mySensors;
#define RLS 19
#define LLS 2
#define debounceTime 30


#define ROUTE_C 4
#define ROUTE_B 2
#define ROUTE_A 1

const PROGMEM uint8_t recv_pin = 11; //Pin no.
IRrecv ir_rx(recv_pin);             //Constructs IRrecv obj. to pin specified
uint8_t  routeNo = 0;                //Rest of code will reference routeNo.
decode_results results;

double lastPressed=0;
void taskTwo(uint8_t firstDecision);

//---------------construction of individual motors(PWM,digital, digital)---------------

Motor motor1(7, 26, 22);
Motor motor2(8, 27, 50);
Motor motor3(4, 28, 24);
Motor motor4(5, 29, 25);

//----------------------construction of motor controllers---------------------
motorController myController(motor1, motor2, motor3, motor4);

// volatile data types denote variables changed within interrupts
volatile int taskNumber = 0;
void changeTask();
void setup() {
  Serial.begin(9600);
  //setting
  pinMode(RLS, INPUT_PULLUP);
  pinMode(LLS, INPUT_PULLUP);

  attachInterrupt(digitalPinToInterrupt(2), changeTask, FALLING);
  attachInterrupt(digitalPinToInterrupt(19), changeTask, FALLING);
  mySensors.sensorSetUp();

}

void loop() {
  switch (taskNumber) {
    case 0:
      taskOne();
      break;
    case 1:
      taskTwo(routeNo & ROUTE_A);
      break;
    case 2:
      myController.haults();
  }


}



void taskOne() {
  // ----- /!\ ----- [Module: IR] BEGIN COPY INTO TASK 0
  ir_rx.enableIRIn();   // Start the receiver
  routeNo = getIRcmd(); // Uses best out of five "voting" for error correction
  // ----- /!\ ----- [Module:IR] END COPY INTO TASK 0
  Serial.print("Recv code: ");
  Serial.println(routeNo);
  taskNumber++;
}



//----------------------the interrupt service rutine----------
void changeTask() {
<<<<<<< HEAD
 tasknumber++;
=======
  if (millis() - lastPressed > debounceTime) {
    lastPressed = millis();
    taskNumber++;
>>>>>>> 02792d64eb74ec1b608fcc2a9af52b7ff234a307
}

// ----- /!\ ----- [Module: IR] COPY THESE FUNCTIONS INTO FINAL
uint8_t getIRcmd()
{
  uint8_t retVal = 0;                         //Will remain zero if no commands recv; failsafe route.
  uint8_t inputBuf;                           //Temporary storage indicating the path received.
  uint8_t numCounts[8] = {0};                 //Creates 8-wide array, num of received commands, idx=cmd

  //Decodes five signals from IR and uses them to tally "votes" for which command was received.
  for (uint8_t i = 0; i < 5; i++) {
    inputBuf = readIR();                    //Gets last byte received over IR.
    if (i < 4) ir_rx.resume();              //Listen only five times; does not resume on final run
    if (inputBuf < 8) numCounts[inputBuf]++; //Only eight(0-7) states; others are received in error
    //Uses inputBuf to select which slot to inc.
  }

  //Runs through the eight commands, updating retVal when a higher value is found.
  for (uint8_t i = 1; i < 8; i++) {
    if (numCounts[retVal] < numCounts[i]) retVal = i;
  }

  //Off to see the wizard...
  return retVal;
}

uint8_t readIR()
{
  uint32_t startMillis = millis() ; // Time for 1s per attempt

  while (true) {                           // Busywait for a command for 1000ms
    if (ir_rx.decode(&results)) {        // Did we decode a command?
      if (results.decode_type == IEEE) { // No, really; did we? Rejects garbled cmds.
        return results.value;
      }
      return 128;                      // FAIL: command was garbled.
    }
    if (millis() - startMillis > 1000) { // Have we waited for too long?
      return 127;                        // FAIL: waited over 1s to decode.
    }
  }
}
// ----- /!\ ----- [Module: IR] END MATERIAL TO COPY

void taskTwo(int firstBit) {
  if (firstBit) {
    Serial.println("in 0");
    taskNumber = 0;
    while (true) {
      if (mySensors.getSensor1() < 15 ) {
        myController.forwards();
      }
      else if (mySensors.getSensor1() > 24) {
        myController.backwards();
      }else{
      myController.left();
	  }
    }
  }
  else {
    Serial.println("in 1");
    while (true) {
      if (mySensors.getSensor1() < 15 ) {
        myController.forwards();
      }
      else if (mySensors.getSensor1() > 24) {
        myController.backwards();
      }
	  else{
      myController.right();
	  }
    }
  }
}
