#include "motorController.h"
#include "sensorArray.h"
#define RLS 19
#define LLS 2

sensorArray mySensors;
Motor motor1(7, 26, 22);
Motor motor2(8, 27, 50);
Motor motor3(4, 28, 24);
Motor motor4(5, 29, 25);

motorController myController(motor1, motor2, motor3, motor4);

volatile bool taskNumber = 0;
int pastTime = 0;
int currentTime = millis();
int elapseTime = 0;
int stage = 0;
void changeTask();
void beginning();

void setup() {
  Serial.begin(9600);
  pinMode(RLS, INPUT_PULLUP);
  pinMode(LLS, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(2), changeTask, FALLING);
  attachInterrupt(digitalPinToInterrupt(19), changeTask, FALLING);
  mySensors.sensorSetUp();

}

void loop() {
  if (stage == 0) {
    int x = 1;
    beginning(x);
    Serial.println(mySensors.getSensor1());
  }
  if (stage == 1) {
    myController.haults();
  }
}

void changeTask() {

  taskNumber = !taskNumber;
  if (taskNumber == true) {
    while (mySensors.getSensor4() < 52) {
      if (mySensors.getSensor1() < 18 ) {
        myController.forwards();
      }
      else if(mySensors.getSensor1() > 24) {
        myController.backwards();
      } else {
        myController.right();
      }
    }
    stage++;
  } if (taskNumber == false) {gi
    while (mySensors.getSensor2() < 52) {
      if (mySensors.getSensor1() < 20 ) {
        myController.forwards();
      }
      else if (mySensors.getSensor1() > 24) {
        myController.backwards();
      } else {
        myController.left();
      }

    }
    stage++;
  }
}

void beginning(int x) {

  if (x == 0) {
    Serial.println("in 0");
    taskNumber = 0;
    while (true) {
      if (mySensors.getSensor1() < 20 ) {
        myController.forwards();
      }
      else if (mySensors.getSensor1() > 24) {
        myController.backwards();
      }
      else {
        Serial.println(mySensors.getSensor1());
        myController.left();
      }
    }
  }

  else if (x == 1) {
    Serial.println("in 1");
    while (true) {
      if (mySensors.getSensor1() < 20 ) {
        myController.forwards();
      }
      else if (mySensors.getSensor1() > 24) {
        myController.backwards();
      }
      else {
        Serial.println(mySensors.getSensor1());
        myController.right();
      }
    }
  }
}



